package nl.utwente.di.bookQuote;

import java.util.HashMap;

public class Quoter {

    public double getBookPrice(String temp) {
        try {
            int c = Integer.parseInt(temp);
            return (c * 9/5) + 32;
        } catch (NumberFormatException e) {
            return 0.0;
        }
    }
}
